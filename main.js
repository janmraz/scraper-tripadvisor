const express = require('express');
const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const http = require('http');
const clui = require('clui');

const app = express();
const keepAliveAgent = new http.Agent({ keepAlive: true,gzip: true, maxSockets: 510, });
const Progress = clui.Progress;

const BASE_URL = "http://www.tripadvisor.cz";
const PORT = 7000;

// to change
const STARTER_URL = "https://www.tripadvisor.cz/Hotels-g187768-Italy-Hotels.html";
const SPECIFIC_URL = "/Hotels-g295424-Dubai_Emirate_of_Dubai-Hotels.html";
const IDENTIFICATOR = "peking";

app.get('/url',async function (req, res) {
    console.log('SPECIFIC PHASE - start');
    let ok = await doDetailsRequest(SPECIFIC_URL);
    if (ok.error) {
        console.log('error', ok.error);
    }
    fs.writeFile('output.json', JSON.stringify(ok.ok, null, 4), function (err) {
        console.log('details saved to output.json!');
        console.log('SPECIFIC PHASE - done');
    });
});
app.get('/', function (req, res) {
    console.log('INITIAL PHASE - start');
    res.send('INITIAL PHASE - start');
    request(STARTER_URL,async function (error, response, html) {
      try{
        if (!error) {
            var $ = cheerio.load(html);
            var pages = [];
            var cities = [];
            var final = $('.pageNumbers a')[$('.pageNumbers a').length - 1].attribs.href;
            var number = parseInt(final.split('oa')[1].toString());
            console.log('final number of all pages -',number);
            var total = number / 20;
            var base = $('.pageNumbers a')[0].attribs.href;
            for(var i = 1;i <= total;i++){
                pages.push({url: base.replace("oa20", "oa"+20*i) });
            }
            $('#LEAF_GEO_LIST a').each(function (){
                cities.push({url: $(this).attr('href')})
            });
            await Promise.all(pages.map(async (t) => {
                let ok = await doCitiesPageRequest(t.url);
                if (ok.error) {
                    console.log('error', ok.error);
                }
                console.log('cities got from last page -',ok.ok.length);
                cities = cities.concat(ok.ok);
            })).then(() => {
                fs.writeFile('cities.json', JSON.stringify(cities, null, 4), function (err) {
                    console.log('cities saved to cities.json!');
                    console.log('INITIAL PHASE - done');
                });
            });
        }
      }catch(error) {
        console.log('error', error);
      }
    });
});
app.get('/cities',async function (req, res) {
    console.log('CITIES PHASE - start');
    var citiesJSON = require('./cities.json');
    res.send('CITIES PHASE - start');
    let totalResult = [];
    var arrays = [], size = 30;
    console.log('total cities -',citiesJSON.length);
    console.log('dividing cities in smaller chunks');
    while (citiesJSON.length > 0){
        arrays.push(citiesJSON.splice(0, size));
    }
    console.log('chunks length -',arrays.length);
    var details = [];
    var done = 0;
    for(var p = 0;p < arrays.length;p++) {
        await Promise.all(arrays[p].map(async (t) => {
            let ok = await doDetailsRequest(t.url);
            if (ok.error) {
                console.log('error', ok.error);
            }
            details = details.concat(ok.ok);
            clearline();
            console.log('details length -', details.length, ', last page -', t.url, ', total pages done -', ++done);
        })).then(async ()=>{
            await sleep(3000);
            console.log('DONE ARRAY -',p,',ALL -',arrays.length);
            if(p === (arrays.length - 1)){
                fs.writeFile('output.json', JSON.stringify(details, null, 4), function (err) {
                    console.log('details saved to output.json!');
                    console.log('CITIES PHASE - done');
                });
            }
        });
    }
});
app.get('/done',async function (req, res) {
    console.log('FINAL PHASE - start');
    res.send('FINAL PHASE - start');
    var detailsJSON = require('./output.json');
    let totalResult = [];
    var chunks = [], size = 30;
    var total = detailsJSON.length;
    console.log('total details',detailsJSON.length);
    console.log('dividing details in smaller chunks');
    while (detailsJSON.length > 0){
        chunks.push(detailsJSON.splice(0, size));
    }
    console.log('chunks length -',chunks.length);
    let done = 0;
    for(var p = 0;p < chunks.length;p++) {
        printProgressPercent(done == 0 ? 0 : Math.floor((done / total) * 100)/100);
        await Promise.all(chunks[p].map(async (t) => {
            if(t) {
                clearline();
                process.stdout.write('calling ' + t.url);
                printProgressPercent(done == 0 ? 0 : Math.floor((done / total) * 100) / 100);
                let ok = await doRequest(t.url);
                if (ok.error || ok.ok == null) {
                    console.log('error', ok.error);
                } else {
                  totalResult.push(ok.ok);
                }
            }else{
                console.log('error, t is null');
            }
            clearline();
            done++;
            console.log('total length -', totalResult.length, 'page', t ? t.url : '', 'done', done);
            printProgressPercent(done == 0 ? 0 : Math.floor((done / total) * 100)/100);
        })).then(() => {
            clearline();
            process.stdout.write('done ' + p + ' / ' + chunks.length);
            printProgressPercent(done == 0 ? 0 : Math.floor((done / total) * 100)/100);
        });
        await sleep(3000);
        clearline();
        console.log('DONE',p + 1,'of',chunks.length);
        if(p % 50 === 0) {
            fs.writeFile('result.' + IDENTIFICATOR + '.json', JSON.stringify(totalResult), function (err) {
                if (err) console.log(err);
                clearline();
                console.log('File successfully written! - (prematurely) Check your project directory for the result.[type].json file');
            });
        }
        if(p === (chunks.length - 1)){
            fs.writeFile('result.' + IDENTIFICATOR + '.json', JSON.stringify(totalResult), function (err) {
                if(err) console.log(err);
                console.log('FINAL PHASE - done');
                console.log('File successfully written! - Check your project directory for the result.[type].json file');
            });
        }
    }
});
app.get('/emails',function(req,res) {
  var resultJSON = require('./result.' + IDENTIFICATOR + '.json');
  var emails = [];
  resultJSON.forEach((h)=>{
      if(h && h.email && h.email !== "" && h.email.length > 0){
          emails = emails.concat(h.email)
      }
  });
  console.log('emails',emails);
  fs.writeFile('emails.' + IDENTIFICATOR + '.json', emails, function (err) {
      if(err) console.log(err);
      console.log('File successfully written! emails');
      res.send(emails);
  });
})
function printProgressPercent(percent){
    var thisProgressBar = new Progress(30);
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(thisProgressBar.update(percent));
}
function printProgress(current,total){
    var thisProgressBar = new Progress(total);
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(thisProgressBar.update(current, total));
}
function clearline() {
    process.stdout.write("\r\x1b[K")
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function extractEmails (text) {
    try{
        return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    }catch (err){
        clearline();
        console.log("error catched in extractEmails",err);
        return null;
    }
}
function extractUrl (text) {
    try {
        var regex = /maps.google.com\/maps\/api\/staticmap[\s\S]*="/gi, result, value = [];
        result = regex.exec(text);
        value = result[0].split('&center=')[1].split('&')[0].split(',');
        return value;
    } catch (err){
        clearline();
        console.log("error catched in extractUrl",err,result);
        return [];
    }
}
function doRequest(url) {
    return new Promise(function (resolve, reject) {
        request({
            uri: url,
            baseUrl: BASE_URL,
            agent: keepAliveAgent
        },async function (error, res, body) {
            if (!error && res && res.statusCode == 200) {
                try {
                    var result = {};
                    var $ = cheerio.load(body);
                    var title = $("#HEADING").text();
                    var website = '';
                    var email = '';
                    var address = $('.address').text();
                    var basic = extractUrl(body);
                    var long = basic.length > 1 ? basic[1] : '';
                    var lat = basic.length  > 1 ? basic[0] : '';
                    let number = parseInt(url.split('-d')[1]);
                    await request("https://www.tripadvisor.cz/ShowUrl?&excludeFromVS=false&odc=BusinessListingsUrl&d="+number+"&url=1",function (error, res, body) {
                        try {
                            if (!error && res) {
                                var $ = cheerio.load(body);
                                if ($('title').text()[0] + $('title').text()[1] + $('title').text()[2] !== '404') {
                                    website = res.request.href;
                                    email = extractEmails($("body").text());
                                }
                                result = {title, address, long, lat, website, email};
                            } else {
                                console.log('error in second detail request', error);
                                resolve({error: error});
                            }
                            resolve({ok: result});
                        }catch (err){
                            console.log('catched error in  second detail request', error);
                        }
                    });

                }catch(err){
                    clearline();
                    console.log('error catched second detail request', err);
                    resolve({error: error});
                }
            } else {
                console.log('error in detail request',error);
                resolve({error: error});
            }
        });
    })
}
function doPageRequest(url) {
    return new Promise(function (resolve, reject) {
        request(BASE_URL + url, function (error, res, body) {
            if (!error && res && res.statusCode == 200) {
                var result = [];
                var $ = cheerio.load(body);
                $('.listing_title a').each(function (){
                    result.push({url: $(this).attr('href')})
                });
                resolve({ok: result});
            } else {
                console.log('error!!!!',error);
                resolve({error: error});
            }
        });
    })
}
function doCitiesPageRequest(url) {
    return new Promise(function (resolve, reject) {
        request(BASE_URL + url, function (error, res, body) {
            if (!error && res && res.statusCode == 200) {
                var result = [];
                var $ = cheerio.load(body);
                console.log('l',$('#BROAD_GRID a').length,BASE_URL + url);
                $('#BROAD_GRID a').each(function (){
                    result.push({url: $(this).attr('href')})
                });
                resolve({ok: result});
            } else {
                console.log('error!!!!',error);
                reject({error: error});
            }
        });
    })
}
function doDetailsRequest(url) {
    return new Promise(function (resolve, reject) {
        try {
            console.log('start');
            request(BASE_URL + url, async function (error, response, html) {
                try {
                    clearline();
                    console.log('return');
                    if (!error) {
                        var $ = cheerio.load(html);
                        var details = [];
                        var pages = [];
                        $('.titleBox a').each(function () {
                            details.push({url: $(this).attr('href')})
                        });
                        if ($('.pageNumbers a').length > 1) {
                          try{
                            var final = $('.pageNumbers a')[$('.pageNumbers a').length - 1].attribs.href;
                          }catch(err){
                            console.log('error found. probably')
                          }
                            var number = parseInt(final.split('oa')[1].toString());
                            console.log('final number', number);
                            var total = number / 30;
                            var base = $('.pageNumbers a')[0].attribs.href;
                            for (var i = 1; i <= total; i++) {
                                pages.push({url: base.replace("oa30", "oa" + 30 * i)});
                            }
                            await Promise.all(pages.map(async (t) => {
                                try {
                                    console.log('going to PAgeReq',t.url);
                                    let ok = await doPageRequest(t.url);
                                    if (ok.error) {
                                        console.log('error', ok.error);
                                    }
                                    console.log('concating result',ok);
                                    details = details.concat(ok.ok);
                                }catch (err){
                                    console.log('error in pages promise',err);
                                }
                            }));
                        }
                        console.log('done', details.length);
                        resolve({ok: details});
                    }
                }catch (err){
                    console.log('error in doDetailsRequest',err);
                    resolve({error: err});
                }
            });
        }catch (err){
            console.log('error in doDetailsRequest',err);
            resolve({error: err});
        }
    })
}

app.listen(PORT);
console.log('Magic happens on port',PORT);
exports = module.exports = app;
